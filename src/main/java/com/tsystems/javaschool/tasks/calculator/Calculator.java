package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;

public class Calculator {
    private final String OPERATORS = "+-*/";

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        Stack<Double> nums = new Stack<>();

        try{
            List<String> postfixExpr = toPostfixNotation(statement);
            for (String token : postfixExpr) {

                if (!isOperator(token)) {
                    nums.push(Double.parseDouble(token));
                } else {
                    double d1 = nums.pop();
                    double d2 = nums.pop();

                    switch (token) {
                        case "+":
                            nums.push(d1 + d2);
                            break;
                        case "-":
                            nums.push(d2 - d1);
                            break;
                        case "*":
                            nums.push(d2 * d1);
                            break;
                        case "/":
                            if (d1 == 0) {
                                throw new ArithmeticException();
                            }
                            nums.push(d2 / d1);
                            break;
                    }
                }
            }

            double numRes = nums.pop();
            return output(numRes);

        } catch (Exception e) {
            return null;
        }
    }

    private List<String> toPostfixNotation(String s) {
        String expr = s.replace(" ", "");
        StringTokenizer stringTokenizer = new StringTokenizer(expr, OPERATORS + "()", true);
        List<String> outputList = new ArrayList<>();
        Stack<String> operatorStack = new Stack<>();

        while (stringTokenizer.hasMoreTokens()) {
            String token = stringTokenizer.nextToken();

            if(isNumber(token)) {
                outputList.add(token);

            } else if(token.equals("(")) {
                operatorStack.push(token);

            } else if(isOperator(token)) {
                while(!operatorStack.empty() && (getPrecedence(operatorStack.peek()) >= getPrecedence(token))
                        && !operatorStack.peek().equals("(")) {
                    outputList.add(operatorStack.pop());
                }
                operatorStack.push(token);
            } else if(token.equals(")")) {
                while (!operatorStack.peek().equals("(")) {
                    outputList.add(operatorStack.pop());
                }
                if (operatorStack.peek().equals("(")) {
                    operatorStack.pop();
                }
            }
        }

        while (!operatorStack.isEmpty()) {
            outputList.add(operatorStack.pop());
        }

        return outputList;
    }

    private String output(double d) {
        String result = "";
        if(d % 1 == 0) {
            result = Long.toString(Math.round(d));
        } else {
            result = String.format("%.4f", d);
            result = result.replace(",", ".");
            result = result.replaceAll("(0+$)", "");
        }
        return result;
    }

    private boolean isNumber(String s) {
        if (s.matches("[0-9]+(\\.[0-9]+)?")){
            return true;
        } else {
            return false;
        }
    }

    private byte getPrecedence(String s) {
        if (s.equals("+") || s.equals("-")) {
            return 1;
        }
        return 2;
    }

    private boolean isOperator(String s) {
        return OPERATORS.contains(s);
    }
}