package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        List<Integer> input = inputNumbers;

        if(input.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        try {
            Collections.sort(input);
        } catch(OutOfMemoryError outOfMemoryError) {
            throw new CannotBuildPyramidException();
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }

        int lengthOfArr = input.size();
        double floor = (-1 + Math.sqrt(1 + 8 * lengthOfArr)) / 2;

        if (floor % 1 != 0) {
            throw new CannotBuildPyramidException();
        }

        int numOfColumns = (int) (2 * floor - 1);
        int numOfLines = (int) floor;
        int[][] pyramid = new int[numOfLines][numOfColumns];
        int iter = 0;

        for(int i = 0; i < numOfLines; i++) {
            int k = numOfLines - 1 - i;
            int z = i + 1;
            while(z > 0) {
                pyramid[i][k] = input.get(iter);
                z--;
                k += 2;
                iter++;
            }
        }

        return pyramid;
    }
}